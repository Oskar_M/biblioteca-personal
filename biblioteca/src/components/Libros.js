import React, { Component } from 'react';
import Busqueda from './Busqueda';
import ListaLibro from './ListaLibro';
import request from 'superagent';


class Libros extends Component {
    constructor(props){
        super(props);
        this.state = {
            books: [],
            searchField: ''
        }
    }

    searchBook=(e) =>{
        e.preventDefault();
        request
            .get("https://www.googleapis.com/books/v1/volumes")
            .query({q: this.state.searchField})
            .then((data) =>{ 
                console.log(data);
                //this.setState({books:[...data.body.items]})
            })
    }

    handleSearch = (e) =>{
        
        this.setState({searchField: e.target.value})
    }

    render(){
        return (
        <div>
            <Busqueda searchBook={this.searchBook} handleSearch={this.handleSearch}/>
            <ListaLibro books={this.state.books}/>
        </div>
        );
    }
    
}

export default Libros;
