import React from 'react';

const TarjetaLibro = (props) => {
    return(
       <div className="card-container">
           <img src={props.imagen} alt=""/>
           <div className="desc"> 
                <h2>{props.titulo}</h2>
                <h3>{props.autor}</h3>
                <p>{props.publicado}</p>
           </div>

       </div>
    );
}

export default TarjetaLibro;