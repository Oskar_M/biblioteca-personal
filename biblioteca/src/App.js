import React, { Component } from 'react';
import Header from './components/Header';
import Libros from './components/Libros';

class App extends Component {
  render(){
    return (
      <div className="App">
       <Header/>
       <Libros/>  
      </div>
    );
  }
  
}

export default App;
